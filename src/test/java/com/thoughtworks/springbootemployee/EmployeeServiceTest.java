package com.thoughtworks.springbootemployee;

import com.thoughtworks.springbootemployee.exception.AgeIsInvalidException;
import com.thoughtworks.springbootemployee.exception.NotFoundException;
import com.thoughtworks.springbootemployee.exception.SalaryNotMatchAgeException;
import com.thoughtworks.springbootemployee.model.Employee;
import com.thoughtworks.springbootemployee.repository.EmployeeRepository;
import com.thoughtworks.springbootemployee.service.EmployeeService;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

class EmployeeServiceTest {
    @Test
    void should_not_create_successfully_when_create_employee_given_age_is_invalid() {
        EmployeeService employeeService = new EmployeeService(mock(EmployeeRepository.class));
        Employee employee = new Employee(1L, "sj", 15, "Female", 6000d);
        //given(employeeRepository.insert(any())).willReturn(employee);
        assertThrows(AgeIsInvalidException.class,()->employeeService.create(employee));
    }
    @Test
    void should_not_create_successfully_when_create_employee_given_salary_not_match_age() {
        EmployeeService employeeService = new EmployeeService(mock(EmployeeRepository.class));
        Employee employee = new Employee(1L, "sj", 33, "Female", 6000d);
        //given(employeeRepository.insert(any())).willReturn(employee);
        assertThrows(SalaryNotMatchAgeException.class,()->employeeService.create(employee));
    }

    @Test
    void should_create_successfully_when_create_employee_given_age_is_valid() {
        EmployeeRepository employeeRepository = mock(EmployeeRepository.class);
        Employee employee = new Employee(1L, "dz", 20, "Male", 6000d);
        employee.setActive(true);
        given(employeeRepository.insert(any())).willReturn(employee);

        EmployeeService employeeService = new EmployeeService(employeeRepository);
        Employee createdEmployee = employeeService.create(employee);

        assertThat(createdEmployee.getId()).isEqualTo(1);
        assertThat(createdEmployee.getName()).isEqualTo("dz");
        assertThat(createdEmployee.getSalary()).isEqualTo(6000);
        assertThat(createdEmployee.getGender()).isEqualTo("Male");
        assertThat(createdEmployee.isActive()).isEqualTo(true);

        verify(employeeRepository).insert(argThat(employee1 -> {
            assertThat(createdEmployee.getId()).isEqualTo(1);
            assertThat(createdEmployee.getName()).isEqualTo("dz");
            assertThat(createdEmployee.getSalary()).isEqualTo(6000);
            assertThat(createdEmployee.getGender()).isEqualTo("Male");
            assertThat(createdEmployee.isActive()).isEqualTo(true);
            return true;
        }));
    }

    @Test
    void should_set_in_active_when_delete_employee_given_an_active_employee(){
        EmployeeRepository mock = mock(EmployeeRepository.class);
        EmployeeService employeeService = new EmployeeService(mock);
        Employee john = new Employee(1L, "John Smith", 32, "Male", 5000d);
        john.setActive(true);
        given(mock.findById(1L)).willReturn(john);
        employeeService.delete(1L);
        john.setActive(false);
        verify(mock,times(1)).update(john);
    }

    @Test
    void should_verify_successfully_when_create_employee_given_active_is_valid() {
        EmployeeRepository employeeRepository = mock(EmployeeRepository.class);
        Employee employee = new Employee(1L, "dz", 30, "Male", 8000d);
        employee.setActive(true);
        Employee newEmployee = new Employee(1L, "dz", 20, "Male", 6000d);
        EmployeeService employeeService = new EmployeeService(employeeRepository);
        given(employeeRepository.insert(any())).willReturn(employee);
        given(employeeRepository.findById(1L)).willReturn(employee);
        given(employeeRepository.update(employee)).willReturn(newEmployee);
        Employee updatedEmployee = employeeService.update(1L,newEmployee);
        assertThat(updatedEmployee.getId()).isEqualTo(1);
        assertThat(updatedEmployee.getName()).isEqualTo("dz");
        assertThat(updatedEmployee.getSalary()).isEqualTo(6000);
        assertThat(updatedEmployee.getGender()).isEqualTo("Male");

        verify(employeeRepository).update(argThat(employee1 -> {
            assertThat(updatedEmployee.getId()).isEqualTo(1);
            assertThat(updatedEmployee.getName()).isEqualTo("dz");
            assertThat(updatedEmployee.getSalary()).isEqualTo(6000);
            assertThat(updatedEmployee.getGender()).isEqualTo("Male");
            return true;
        }));
    }
    @Test
    void should_verify_not_successfully_when_create_employee_given_active_is_valid() {
        EmployeeRepository employeeRepository = mock(EmployeeRepository.class);
        Employee employee = new Employee(1L, "dz", 30, "Male", 8000d);
        employee.setActive(false);
        Employee newEmployee = new Employee(1L, "dz", 20, "Male", 6000d);
        EmployeeService employeeService = new EmployeeService(employeeRepository);
        given(employeeRepository.insert(any())).willReturn(employee);
        given(employeeRepository.findById(1L)).willReturn(employee);
        assertThrows(NotFoundException.class,()->employeeService.update(1L,newEmployee));
    }
}
package com.thoughtworks.springbootemployee;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.thoughtworks.springbootemployee.model.Company;
import com.thoughtworks.springbootemployee.repository.CompanyRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.hamcrest.Matchers.hasSize;

@SpringBootTest
@AutoConfigureMockMvc
public class CompanyControllerTest {
    @Autowired
    MockMvc mockMvc;
    @Autowired
    CompanyRepository companyRepository;

    @BeforeEach
    void cleanup() {
        companyRepository.clearAll();
    }

    @Test
    void should_return_all_companies_when_call_get_method() throws Exception {
        Company company = new Company();
        company.setId(1L);
        company.setName("Company One");
        companyRepository.insert(company);

        mockMvc.perform(MockMvcRequestBuilders.get("/companies"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value("Company One"));
    }

    @Test
    void should_return_specific_company_when_call_get_method_with_id() throws Exception {
        Company company = new Company();
        company.setId(1L);
        company.setName("Company One");
        companyRepository.insert(company);

        mockMvc.perform(MockMvcRequestBuilders.get("/companies/{id}", company.getId()))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("Company One"));
    }
    @Test
    void should_return_newly_created_company_when_post_method() throws Exception {
        Company company = new Company();
        company.setId(1L);
        company.setName("Company One");
        String companyJson = new ObjectMapper().writeValueAsString(company);

        mockMvc.perform(MockMvcRequestBuilders.post("/companies")
                .contentType(MediaType.APPLICATION_JSON)
                .content(companyJson))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("Company One"));
    }

    @Test
    void should_return_updated_company_when_put_method() throws Exception {
        Company company = new Company();
        company.setId(1L);
        company.setName("Company One");
        companyRepository.insert(company);

        Company updatedCompany = new Company();
        updatedCompany.setName("Updated Company Name");
        String updatedCompanyJson = new ObjectMapper().writeValueAsString(updatedCompany);

        mockMvc.perform(MockMvcRequestBuilders.put("/companies/{id}", company.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(updatedCompanyJson))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("Updated Company Name"));
    }

    @Test
    void should_return_no_content_when_delete_company_given_valid_id() throws Exception {
        Company company = new Company();
        company.setId(1L);
        company.setName("Company One");
        companyRepository.insert(company);

        mockMvc.perform(MockMvcRequestBuilders.delete("/companies/{id}", company.getId()))
                .andExpect(MockMvcResultMatchers.status().isNoContent());
    }
}

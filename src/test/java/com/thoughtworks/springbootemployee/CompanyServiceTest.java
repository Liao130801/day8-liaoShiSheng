package com.thoughtworks.springbootemployee;

import com.thoughtworks.springbootemployee.model.Company;
import com.thoughtworks.springbootemployee.repository.CompanyRepository;
import com.thoughtworks.springbootemployee.service.CompanyService;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.*;
import static org.assertj.core.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class CompanyServiceTest {

    @Test
    void should_return_company_when_call_create_given_company() {
        // Given
        CompanyRepository companyRepository = mock(CompanyRepository.class);
        CompanyService companyService = new CompanyService(companyRepository);
        Company company = new Company();
        company.setName("Company1");
        when(companyRepository.insert(any())).thenReturn(company);

        // When
        Company result = companyService.insert(company);

        // Then
        assertNotNull(result);
        verify(companyRepository).insert(any(Company.class));
    }

    @Test
    void should_return_all_companies_when_call_getAll() {
        // Given
        CompanyRepository companyRepository = mock(CompanyRepository.class);
        CompanyService companyService = new CompanyService(companyRepository);

        // When
        companyService.getAll();

        // Then
        verify(companyRepository).getAll();
    }

    @Test
    void should_return_company_when_call_getCompanyById_given_id() {
        // Given
        CompanyRepository companyRepository = mock(CompanyRepository.class);
        CompanyService companyService = new CompanyService(companyRepository);
        long id = 1L;

        // When
        companyService.getCompanyById(id);

        // Then
        verify(companyRepository).getCompanyById(id);
    }

    @Test
    void should_return_company_when_call_updateById_given_id_and_company() {
        // Given
        CompanyRepository companyRepository = mock(CompanyRepository.class);
        CompanyService companyService = new CompanyService(companyRepository);
        long id = 1L;
        Company company = new Company();
        company.setName("Company1");

        // When
        companyService.updateById(id, company);

        // Then
        verify(companyRepository).updateById(id, company);
    }

    @Test
    void should_execute_once_when_call_deleteById_given_id() {
        // Given
        CompanyRepository companyRepository = mock(CompanyRepository.class);
        CompanyService companyService = new CompanyService(companyRepository);
        long id = 1L;

        // When
        companyService.deleteById(id);

        // Then
        verify(companyRepository).deleteById(id);
    }

    @Test
    void should_return_employee_list_when_call_getEmployeeList_given_company_id() {
        // Given
        CompanyRepository companyRepository = mock(CompanyRepository.class);
        CompanyService companyService = new CompanyService(companyRepository);
        long companyId = 1L;

        // When
        companyService.getEmployeeList(companyId);

        // Then
        verify(companyRepository).getEployeeList(companyId);
    }

    @Test
    void should_return_companies_list_when_call_getCompanyListByPage_given_page_and_size() {
        // Given
        CompanyRepository companyRepository = mock(CompanyRepository.class);
        CompanyService companyService = new CompanyService(companyRepository);
        int page = 1, size = 5;

        // When
        companyService.getCompanyListByPage(page, size);

        // Then
        verify(companyRepository).getCompanyListByPage(page, size);
    }
}

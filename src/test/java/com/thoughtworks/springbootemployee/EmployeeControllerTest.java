package com.thoughtworks.springbootemployee;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.thoughtworks.springbootemployee.model.Employee;
import com.thoughtworks.springbootemployee.repository.EmployeeRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;


@SpringBootTest
@AutoConfigureMockMvc
public class EmployeeControllerTest {
    @Autowired
    MockMvc client;
    @Autowired
    EmployeeRepository employeeRepository;

    @BeforeEach
    void cleanEmployeeData() {
        employeeRepository.clearAll();
    }


    @Test
    void should_return_all_employees_when_perform_getAllEmployees_given_employees() throws Exception {
        //given
        Employee employee = new Employee(1L, "Lily", 20, "male", 3000.0);
        employeeRepository.insert(employee);
        //when  then
        client.perform(MockMvcRequestBuilders.get("/employees"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$",hasSize(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value("Lily"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].age").value(20))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].gender").value("male"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].salary").value(3000.0));
    }

    @Test
    void should_return_ok_when_perform_getEmployeeById_given_employees() throws Exception{
        //given
        Employee employee = new Employee(1L, "Lily", 20, "male", 3000.0);
        employeeRepository.insert(employee);
        //when  then
        client.perform(MockMvcRequestBuilders.get("/employees/{id}",employee.getId()))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("Lily"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.age").value(20))
                .andExpect(MockMvcResultMatchers.jsonPath("$.gender").value("male"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.salary").value(3000.0));
    }
    @Test
    void should_return_isCreated_when_perform_create_employee_given_employees() throws Exception{
        //given
        Employee employee = new Employee(1L, "Lily", 20, "male", 3000.0);
        employee.setActive(true);
        String employeeJson = new ObjectMapper().writeValueAsString(employee);
        //when then
        client.perform(MockMvcRequestBuilders.post("/employees")
                .contentType(MediaType.APPLICATION_JSON)
                .content(employeeJson))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("Lily"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.age").value(20))
                .andExpect(MockMvcResultMatchers.jsonPath("$.gender").value("male"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.salary").value(3000.0))
                .andExpect(MockMvcResultMatchers.jsonPath("$.active").value(true));
        Employee employeeSaved = employeeRepository.findById(employee.getId());
        Assertions.assertEquals(employee.getId(),employeeSaved.getId());
        Assertions.assertTrue(employeeSaved.isActive());
    }
    @Test
    void should_return_employees_filter_by_gender_when_perform_findEmployeesByGender_given_employees() throws Exception {
        //given
        Employee employee = new Employee(1L, "Lily", 20, "male", 3000.0);
        employeeRepository.insert(employee);
        //when  then
        client.perform(MockMvcRequestBuilders.get("/employees?gender={gender}",employee.getGender()))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$",hasSize(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value("Lily"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].age").value(20))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].gender").value("male"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].salary").value(3000.0));
    }
    @Test
    void should_get_updated_employee_when_perform_update_employee_given_employees() throws Exception {
        //given
        Employee employee = new Employee(1L, "Lily", 20, "male", 3000.0);
        Employee newEmployee = new Employee(1L, "Lily", 30, "male", 5000.0);
        employee.setActive(true);
        newEmployee.setActive(true);
        employeeRepository.insert(employee);
        String employeeJson = new ObjectMapper().writeValueAsString(newEmployee);
        //when

        //then
        client.perform(MockMvcRequestBuilders.put("/employees/{id}",employee.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(employeeJson))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("Lily"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.age").value(30))
                .andExpect(MockMvcResultMatchers.jsonPath("$.gender").value("male"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.salary").value(5000.0));
    }
    @Test
    void should_return_no_content_when_perform_delete_employee_given_employees() throws Exception {
        //given
        Employee employee = new Employee(null, "Lily", 20, "male", 3000.0);
        Employee employee2 = new Employee(null, "Lily", 20, "male", 3000.0);
        employee.setActive(true);
        employee2.setActive(true);
        employeeRepository.insert(employee);
        employeeRepository.insert(employee2);

        //when then
        client.perform(MockMvcRequestBuilders.delete("/employees/{id}",employee.getId()))
                .andExpect(MockMvcResultMatchers.status().isNoContent());
        Employee employee3 = employeeRepository.findById(employee.getId());
        Assertions.assertFalse(employee3.isActive());
    }
    @Test
    void should_return_employees_when_perform_findByPage_given_employees() throws Exception {
        //given
        Employee employee1 = new Employee(1L, "Lily", 20, "male", 3000.0);
        Employee employee2 = new Employee(2L, "Lily", 20, "male", 3000.0);
        employeeRepository.insert(employee1);
        employeeRepository.insert(employee2);
        //when then
        client.perform(MockMvcRequestBuilders.get("/employees?page={page}&size={size}",1,2))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$",hasSize(2)));
    }
}

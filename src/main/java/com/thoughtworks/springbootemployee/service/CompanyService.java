package com.thoughtworks.springbootemployee.service;

import com.thoughtworks.springbootemployee.model.Company;
import com.thoughtworks.springbootemployee.model.Employee;
import com.thoughtworks.springbootemployee.repository.CompanyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CompanyService {
    private final CompanyRepository companyRepository;

    @Autowired
    public CompanyService(CompanyRepository companyRepository) {
        this.companyRepository = companyRepository;
    }

    // 插入公司
    public Company insert(Company newCompany) {
        return companyRepository.insert(newCompany);
    }

    // 查找所有公司
    public List<Company> getAll() {
        return companyRepository.getAll();
    }

    // 通过公司ID查找公司
    public Company getCompanyById(Long companyId) {
        return companyRepository.getCompanyById(companyId);
    }

    // 更新公司信息
    public Company updateById(Long id, Company sourceCompany) {
        return companyRepository.updateById(id, sourceCompany);
    }

    // 删除公司
    public Company deleteById(Long id) {
        return companyRepository.deleteById(id);
    }

    // 获取员工列表
    public List<Employee> getEmployeeList(Long companyId) {
        return companyRepository.getEployeeList(companyId);
    }

    // 分页获取公司列表
    public List<Company> getCompanyListByPage(int page, int size) {
        return companyRepository.getCompanyListByPage(page, size);
    }

}

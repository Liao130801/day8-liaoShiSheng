package com.thoughtworks.springbootemployee.repository;

import com.thoughtworks.springbootemployee.controller.EmployeeController;
import com.thoughtworks.springbootemployee.model.Company;
import com.thoughtworks.springbootemployee.model.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

@Repository
public class CompanyRepository {
    private List<Company> companies = new ArrayList<>();
    private static AtomicLong atomicLongId = new AtomicLong(0);
    private EmployeeRepository employeeRepository;
    @Autowired
    public CompanyRepository(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public Company insert(Company company) {
        company.setId(atomicLongId.incrementAndGet());
        companies.add(company);
        return company;
    }

    public List<Company> getAll() {
        return companies;
    }

    public Company getCompanyById(Long companyId) {
        return companies.stream().filter(company -> Objects.equals(company.getId(),companyId)).findFirst().orElse(new Company());
    }
    public Company updateById(Long id, Company sourceCompany) {
        for (Company company : companies) {
            if (Objects.equals(company.getId(), id)) {
                company.setName(sourceCompany.getName());
                return company;
            }
        }
        return sourceCompany;
    }

    public Company deleteById(Long id) {
        for (Company company : companies) {
            if (Objects.equals(company.getId(), id)) {
                companies.remove(company);
                return company;
            }
        }
        return null;
    }

    public List<Employee> getEployeeList(Long companyId){
        if(companyId == null){
            throw new IllegalArgumentException("Company ID must not be null");
        }
        if(employeeRepository == null){
            throw new IllegalStateException("Employee Repository has not been initialized");
        }
        return employeeRepository.getEmployeeListByCompanyId(companyId);
    }

    public List<Company> getCompanyListByPage(int page, int size) {
        return companies.stream()
                .skip((long) (page - 1) * size)
                .limit(size)
                .collect(Collectors.toList());

    }


    public void clearAll() {
        this.companies.clear();
    }
}

package com.thoughtworks.springbootemployee.controller;

import com.thoughtworks.springbootemployee.model.Company;
import com.thoughtworks.springbootemployee.model.Employee;
import com.thoughtworks.springbootemployee.repository.CompanyRepository;
import com.thoughtworks.springbootemployee.service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/companies")
public class CompanyController {
//    private static final CompanyRepository companyRepository = new CompanyRepository();
private final CompanyService companyService;

    @Autowired
    public CompanyController(CompanyService companyService) {
        this.companyService = companyService;
    }

    @GetMapping
    public ResponseEntity<List<Company>> getAll(){
        return new ResponseEntity<>(companyService.getAll(), HttpStatus.OK);
    }

    @GetMapping("/{companyId}")
    public ResponseEntity<Company> getCompany(@PathVariable Long companyId){
        return new ResponseEntity<>(companyService.getCompanyById(companyId), HttpStatus.OK);
    }

    @GetMapping("/{companyId}/employees")
    public ResponseEntity<List<Employee>> getEmployeeListByCompanyId(@PathVariable Long companyId){
        return new ResponseEntity<>(companyService.getEmployeeList(companyId), HttpStatus.OK);
    }

    @GetMapping("/page")
    public ResponseEntity<List<Company>> getPage(@RequestParam int page,@RequestParam int size){
        return new ResponseEntity<>(companyService.getCompanyListByPage(page, size), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Company> create(@RequestBody Company company) {
        return new ResponseEntity<>(companyService.insert(company), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Company> update(@PathVariable Long id, @RequestBody Company company) {
        return new ResponseEntity<>(companyService.updateById(id, company), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Company> delete(@PathVariable Long id) {
        return new ResponseEntity<>(companyService.deleteById(id),HttpStatus.NO_CONTENT);
    }

}
